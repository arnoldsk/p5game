
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(express.static('public'));
app.set('view engine', 'ejs');

/**
 * Routes
 */
app.get('/', (req, res) => {
    res.render('index');
});

/**
 * Storage
 */
const storage = {
    users: {},
    delays: {},
    canvas: {},
};

/**
 * Libs/helpers
 */
class Player {
    constructor(id, name) {
        this.id = id;
        this.data = {
            id,
            name,
            width: 20,
            height: 20,
            pos: {
                x: storage.canvas.width / 2,
                y: storage.canvas.height / 2
            },
            angle: -90,
            isMoving: false,
            isShooting: false,
        };
    }

    radToDeg(angle) {
        return angle * (180 / Math.PI);
    }

    degToRad(angle) {
        return angle * (Math.PI / 180);
    }

    playerRotate(direction) {
        const speed = 5;

        direction = direction === 'right' ? 1 : -1;

        this.data.angle += speed * direction;

        // Bounds
        if (this.data.angle < -359 || this.data.angle > 359) {
            this.data.angle = 0;
        }
    }

    playerMove() {
        // TODO how do I add friction?
        const angle = this.degToRad(this.data.angle);
        const moveX = Math.cos(angle);
        const moveY = Math.sin(angle);
        const speed = 3;

        this.data.pos.x += moveX * speed;
        this.data.pos.y += moveY * speed;

        // Bounds
        const pos = this.data.pos;
        const playerWidth = this.data.width;
        const playerHeight = this.data.height;
        const margin = ((playerWidth + playerHeight) / 2) / 2;

        const canvasWidth = storage.canvas.width;
        const canvasHeight = storage.canvas.height;

        if (pos.x < -margin) pos.x = canvasWidth + margin;
        if (pos.x > canvasWidth + margin) pos.x = -margin;
        if (pos.y < -margin) pos.y = canvasHeight + margin;
        if (pos.y > canvasHeight + margin) pos.y = -margin;
    }

    playerShoot() {
        // TODO disallow shooting while moving?
        // TODO use lasers, with hitscan?
        // TODO fireRate, equal on client?
    }
}

/**
 * Sockets
 */
io.on('connection', (socket) => {
    /**
     * User has left
     */
    socket.on('disconnect', () => {
        if (storage.users[socket.id]) {
            // Remove the user
            delete storage.users[socket.id];
        }
    });

    const userCount = Object.values(storage.users).length;

    // TODO add nickname step
    socket.on('ready', (canvas) => {
        storage.canvas = canvas;
        storage.users[socket.id] = new Player(socket.id, `Player${userCount + 1}`);
    });

    socket.on('playerRotate', (direction) => {
        if (!storage.users[socket.id]) return;

        storage.users[socket.id].playerRotate(direction);
    });

    socket.on('playerMove start', () => {
        if (!storage.users[socket.id]) return;

        storage.users[socket.id].data.isMoving = true;
    });

    socket.on('playerMove stop', () => {
        if (!storage.users[socket.id]) return;

        storage.users[socket.id].data.isMoving = false;
    });

    socket.on('playerShoot', () => {});

    // Clock
    const gameClock = setInterval(() => {
        if (!storage.users[socket.id]) return;

        for (const user of Object.values(storage.users)) {
            if (storage.users[user.id].data.isMoving) {
                storage.users[user.id].playerMove();
            }
        }

        socket.emit('users', storage.users);
    }, 1000 / 60);
});

http.listen(3000, () => {
    console.log('listening on *:3000');
});
