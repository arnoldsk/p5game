
function setup() {
    frameRate(60);
    rectMode(CENTER);
    angleMode(DEGREES);
    createCanvas(storage.canvas.width, storage.canvas.height);

    storage.shipImage = loadImage('img/asteroids_ship0001.png');
    storage.shipAnimations = [];

    for (let i = 2; i <= 7; i++) {
        const image = loadImage(`img/asteroids_ship000${i}.png`);

        storage.shipAnimations.push(image);
    }
}

function draw() {
    background(0);

    fill(175);
    textSize(12);
    textAlign(LEFT);
    text('Move: W A D\nShoot: left mouse click or spacebar', 10, 20);

    for (const user of Object.values(storage.users)) {
        const ship = createSprite();

        ship.position.x = user.pos.x;
        ship.position.y = user.pos.y;
        ship.rotation = user.angle;

        ship.maxSpeed = 6;
        ship.friction = 1;
        ship.setCollider('circle', 0, 0, (user.width + user.height) / 2);

        // Toggle animation based on moving state
        if (user.isMoving) {
            ship.addImage(random(storage.shipAnimations));
        } else {
            ship.addImage(storage.shipImage);
        }

        // Ship's life is a single frame
        ship.life = 1;

        ship.debug = mouseDown();
        ship.id = user.id;

        storage.ships[user.id] = ship;
    }

    // Set client side collisions
    const ships = Object.values(storage.ships);

    for (const shipA of ships) {
        for (const shipB of ships.reverse()) {
            if (shipA.id !== shipB.id) {
                shipA.collide(shipB);
            }
        }
    }

    // Rotation
    if (keyDown('a')) socket.emit('playerRotate', 'left');
    else if (keyDown('d')) socket.emit('playerRotate', 'right');

    drawSprites();
}

function keyPressed(e) {
    if (e.key === 'w') socket.emit('playerMove start');
}

function keyReleased(e) {
    if (e.key === 'w') socket.emit('playerMove stop');
}
