
const MARGIN = 20;

let bullets;
let shipImage, bulletImage, particleImage;

const userShips = {};
let ship;

function setup() {
    frameRate(60);
    createCanvas(800, 600);

    bulletImage = loadImage('img/asteroids_bullet.png');
    shipImage = loadImage('img/asteroids_ship0001.png');
    particleImage = loadImage('img/asteroids_particle.png');

    bullets = new Group;
}

function draw() {
    background(0);

    fill(175);
    textSize(12);
    textAlign(LEFT);
    text('Move: WAS or arrow keys\nShoot: left mouse click or spacebar while not moving', 10, 20);

    for (const user of Object.values(activeUsers)) {
        drawShip(user);
    }

    if (ship) {
        let update = false;
        let isShooting = false;

        /**
         * If out of bounds enter from the opposite side
         */
        if (ship.position.x < -MARGIN) ship.position.x = width + MARGIN;
        if (ship.position.x > width + MARGIN) ship.position.x = -MARGIN;
        if (ship.position.y < -MARGIN) ship.position.y = height + MARGIN;
        if (ship.position.y > height + MARGIN) ship.position.y = -MARGIN;

        // This will trigger a hit
        // asteroids.overlap(bullets, asteroidHit);

        // This will add collision
        // ship.bounce(asteroids);

        /**
         * Moving
         */
        if (keyDown('a') || keyDown(LEFT_ARROW)) {
            ship.rotation -= 4;
            update = true;
        }
        if (keyDown('d') || keyDown(RIGHT_ARROW)) {
            ship.rotation += 4;
            update = true;
        }

        const isMoving = keyDown('w') || keyDown(UP_ARROW);

        if (isMoving) {
            ship.addSpeed(15, ship.rotation);
            ship.changeAnimation('thrust');
            update = true;
        } else {
            if (ship.getAnimationLabel() !== 'normal') {
                ship.changeAnimation('normal');
                update = true;
            }
        }

        /**
         * Shooting
         */
        if (!isMoving && (mouseIsPressed || keyDown(' '))) {
            drawBullet(ship);

            update = true;
            isShooting = true;
        }

        /**
         * Additional checks whether to update
         */
        // Velocity updates for the friction movement
        if (Math.abs(ship.velocity.x + ship.velocity.y) > 0.01) {
            update = true;
        }

        // Send occasional update every half second
        if (frameCount % 30 === 0) {
            update = true;
        }

        if (update) {
            sendShipUpdate(isShooting);
        }
    }

    drawSprites();
}

function drawBullet(ship) {
    if (frameCount % 10 !== 0) {
        return;
    }

    const bullet = createSprite(ship.position.x, ship.position.y);

    bullet.addImage(bulletImage);
    bullet.setSpeed(10, ship.rotation);
    bullet.life = 40;

    bullets.add(bullet);
}

function sendShipUpdate(isShooting) {
    socket.emit('user update', {
        pos: {
            x: ship.position.x * 100 / width,
            y: ship.position.y * 100 / height,
        },
        rotation: ship.rotation,
        isMoving: ship.getAnimationLabel() !== 'normal',
        isShooting,
    });
}

function mouseReleased() {
    if (ship) {
        sendShipUpdate();
    }
}

function drawShip(user) {
    if (typeof userShips[user.id] === 'undefined') {
        const ship = createSprite();

        ship.position.x = width / (100 / user.pos.x);
        ship.position.y = height / (100 / user.pos.y);

        ship.maxSpeed = 6;
        ship.friction = 0.05;
        ship.setCollider('circle', 0, 0, 20);
        ship.addImage('normal', shipImage);
        ship.addAnimation('thrust', 'img/asteroids_ship0002.png', 'img/asteroids_ship0007.png');

        userShips[user.id] = ship;
    }

    if (user.id === socket.id) {
        ship = userShips[user.id];
    } else {
        const otherShip = userShips[user.id];

        otherShip.position.x = width / (100 / user.pos.x);
        otherShip.position.y = height / (100 / user.pos.y);
        otherShip.rotation = user.rotation;

        if (user.isMoving) {
            otherShip.changeAnimation('thrust');
        } else {
            otherShip.changeAnimation('normal');
        }

        if (user.isShooting) {
            drawBullet(otherShip);
        }
    }
}

function _asteroidHit(asteroid, bullet) {
    let newType = asteroid.type - 1;

    if (newType > 0) {
        createAsteroid(newType, asteroid.position.x, asteroid.position.y);
        createAsteroid(newType, asteroid.position.x, asteroid.position.y);
    }

    for (let i = 0; i < 10; i++) {
        let p = createSprite(bullet.position.x, bullet.position.y);
        p.addImage(particleImage);
        p.setSpeed(random(3, 5), random(360));
        p.friction = 0.95;
        p.life = 15;
    }

    bullet.remove();
    asteroid.remove();
}
